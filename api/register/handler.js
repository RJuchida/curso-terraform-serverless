'use strict';

const AWS = require('aws-sdk')
AWS.config.update({
  region: process.env.AWS_REGION
})

const documentClient = new AWS.DynamoDB.DocumentClient()
const bcryptjs = require('bcryptjs')
const uuid = require('uuid/v4')

module.exports.register = async event => {
  // SEMPRE PARSEAR O RECEBIMENTO DOS VALORES EM JSON
  const body = JSON.parse(event.body)

  await documentClient.put({
    TableName: process.env.DYNAMODB_USERS,
    Item: {
      id: uuid(),
      name: body.name,
      email: body.email,
      password: bcryptjs.hashSync(body.password, 10)
    }
  }).promise()

  return {
    statusCode: 201,
    body: JSON.stringify({message: 'Usuário inserido'})
  }
};
