'use strict';

const AWS = require('aws-sdk')
AWS.config.update({
  region: process.env.AWS_REGION
})

const documentClient = new AWS.DynamoDB.DocumentClient()
const bcryptjs = require('bcryptjs')
const jwt = require('jsonwebtoken')

module.exports.login = async event => {
  // SEMPRE PARSEAR O RECEBIMENTO DOS VALORES EM JSON
  const body = JSON.parse(event.body)

  const params = {
    TableName: process.env.DYNAMODB_USERS,
    IndexName: process.env.EMAIL_GSI,
    KeyConditionExpression: 'email = :email',
    ExpressionAttributeValues: {
      ':email': body.email
    }
  };

  const data = await documentClient.query(params).promise();
  const user = data.Items[0]

  if(user) {
    if(bcryptjs.compareSync(body.password, user.password)) {
      delete user.password
      return {
        statusCode: 200,
        body: JSON.stringify({ token: jwt.sign(user, process.env.JWT_TOKEN) })
      }
    }

    return {
      statusCode: 401,
      body: JSON.stringify({message: 'Usuário e senha inválido'})
    }
  }

  return {
    statusCode: 401,
    body: JSON.stringify({message: 'Usuário e senha inválido'})
  }
};
